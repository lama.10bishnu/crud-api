const express = require('express');
const path = require ('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

mongoose.connect('mongodb://localhost/userdb');

let db = mongoose.connection;

//check for db connection
db.on('error',function (){
    console.log(err);
});

db.once('open',function (){
    console.log("Connected to mongodb")
});

//init app
const app = express();

//Bring in Model
let User = require('./models/userinfo');

//Setting Views Path
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//app.set('view engine','pug');

//


//Body parser  Middleware
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json()
app.use(bodyParser.json());

//Bootstrap
app.use(express.static(path.join(__dirname,'public')));

//Home Route
app.get('/', function(req,res) {

    User.find({}, function (err, users) {
        if (err) {
            console.log(err);
        }
        else {
            console.log(users);

            res.json(users);
        }

     });
    });
//add route /users

app.get('/user', function(req,res){
    res.render('index',{
        title: "User form"
    });
});

app.get('/user/:id', function(req,res){
    console.log("Getting one user info");
    User.findOne({
        _id: req.params.id,

    }).exec(function (err,users) {
        if(err){
            res.send("error occured");
        }
        else{
            console.log(users)
            res.render('signledata', {
                id : users.id,
                name: users.name
            });
        }

    });
});
// add Submit POST route

app.post('/user', function (req,res) {
    let user = new User();
    user.name = req.body.name;
    user.email= req.body.email;
    user.address = req.body.address;
    user.phone = req.body.phone;

    user.save(function (err,users) {
        if (err) {
            console.log(err);

        } else {
            console.log(users);
            res.send(users);
            //
            // res.redirect('/');          //res.redirect to the home directory
        }
    });
});

app.put('/user/:id',function (req,res) {
    User.findOneAndUpdate({
            _id: req.params.id
    },
        { $set: { name: req.body.name}},
        {upsert:true },
        function(err,newUser){
        if (err){
            console.log("error while updating info");
            return res.json({
                ok: false
            })
        }
        else{
            console.log(newUser);
            return res.json({
                ok: true
            })
        }
        });
});

app.delete('/user/:id', (req,res) => {
    User.findOneAndRemove({
        _id: req.params.id
    },  (err,users) => {
        if(err){
            res.send('error deleting');
        }
        else{
            console.log(users);
            return res.json({
                ok: true
            })
        }

    })

})
app.listen(3000, function(){
    console.log("Server started on port 3000");
});