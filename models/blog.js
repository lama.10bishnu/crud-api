let mongoose = require('mongoose');

//Article Schema
let blogSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
    },
    mobile:{
        type: String,
        required: true,
    }
})

let Data = module.exports = mongoose.model('Data', blogSchema)