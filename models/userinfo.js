let mongoose = require('mongoose');

//User Schema

let userSchema = mongoose.Schema({
    name:{
        type: String,
        require: true
    },
    email:{
        type: String,
        require: true,
        unique: true
    },
    address:{
        type: String,
        require: true
    },
    phone:{
        type: String,
        require: true
    }

});

let User = module.exports = mongoose.model('User',userSchema);
